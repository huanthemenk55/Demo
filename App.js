/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {import React, { Component } from 'react';
      import {
          Platform,
          StyleSheet,
          Text,
          View,
          Image,
          TouchableOpacity,
          Dimensions,ImageBackground
      } from 'react-native';
      import sound from "./Image/icons8-speaker-48.png";
      import i1 from "./Image/chair.png";
      import styles from "./styles"
      import bg from "./Image/b6.png"
      export default class One extends Component{
          render() {
              const {text1,text2,text3,text4,
                  view1,view2,view3,view4,view5}
                  =styles;
              return (
                  <ImageBackground source={bg} style={styles.container}>
                      <View style={view1}>
                          <Text style={text1}> Listen and choose the correct answer. </Text>
                      </View>
                      <View style={view2}>
                          <Image source={sound} style={{width:25,height:25}}/>
                          <Text style={text2}> What is this?</Text>
                      </View>
                      <View style={view3}>
                          <Image source={i1} style={{width:120,height:120}}/>
                      </View>
                      <View style={view4}>
                          <Text style={text3}> A. This is a table. </Text>
                          <Text style={text3}> B. This is a chair. </Text>
                          <Text style={text3}> C. This is a lamp. </Text>
                          <Text style={text3}> D. This is a door. </Text>
                      </View>
                      <TouchableOpacity  style={view5}>
                          <Text style={text4}> CHECK </Text>
                      </TouchableOpacity >
                  </ImageBackground>
              );
          }
      }



      return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
