import React, { Component } from 'react';
import {
    Dimensions,
    Platform,
    StyleSheet,
    Text,
    View,
    ImageBackground
} from 'react-native';
import {StackNavigator, TabNavigator,DrawerNavigator} from 'react-navigation';
import One from "./One";
import Two from "./Two";
import Three from "./Three";
import Four from "./Four";
import Five from "./Five";
import FlastList from "./FlastList";
import MyToolTip from "./MyToolTip";
import styles from "./styles";
import Swiper from 'react-native-swiper';
const {width,height} = Dimensions.get('window');
export default class MyApp extends Component{
    render() {
        return (
                <Swiper showsPagination={false}>
                    <MyToolTip/>
                    <FlastList/>
                    <One/>
                    <Two/>
                    <Three/>
                    <Four/>
                    <Five/>
                </Swiper>
        );
    }
}