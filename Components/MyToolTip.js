import React from 'react';
import {
    StyleSheet,
    PixelRatio,
    View,
    Text,
    Alert,
} from 'react-native';

import ToolTip from 'react-native-tooltip';

export default class MyToolTip extends React.Component {

    handleCopyPress = () => {
        Alert.alert(`Copy has been pressed!`);
    };

    handleOtherPress = () => {
        Alert.alert(`Other has been pressed!`);
    };

    handleHide = () => {
        console.log('Tooltip did hide');
    };

    handleShow = () => {
        console.log('tooltip did show');
    };

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center'}}>
                <ToolTip
                    ref='tooltip'
                    actions={[
                        {text: 'Copy', onPress: this.handleCopyPress },
                        {text: 'Other', onPress: this.handleOtherPress }
                    ]}
                    onHide={this.handleHide}
                    onShow={this.handleShow}
                    underlayColor={'blue'}
                    style={{backgroundColor:"yellow"}}
                >
                    <Text style={{fontsize:15}}>
                        Press Here.
                    </Text>
                </ToolTip>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    textinputContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },

});