import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,ImageBackground
} from 'react-native';
import sound from "./Image/icons8-speaker-48.png";
import i1 from "./Image/i2.jpg";
import styles from "./styles"
import bg from "./Image/b6.png"
export default class Two extends Component{
    render() {
        const {text1,text2,text3,text4,
            view1,view2,view3,view4,view5}
            =styles;
        return (
            <ImageBackground source={bg} style={styles.container}>
                <View style={view1}>
                    <Text style={text1}> Listen and choose the correct answer. </Text>
                </View>
                <View style={view2}>
                    <Image source={sound} style={{width:25,height:25}}/>
                    <Text style={text2}> What is this?</Text>
                </View>
                <View style={view3}>
                    <Image source={i1} style={{width:135,height:135}}/>
                </View>
                <View style={view4}>
                    <Text style={text3}> A. This is a couch. </Text>
                    <Text style={text3}> B. This is a chair. </Text>
                    <Text style={text3}> C. This is a lamp. </Text>
                    <Text style={text3}> D. This is a door. </Text>
                </View>
                <TouchableOpacity  style={view5}>
                    <Text style={text4}> CHECK </Text>
                </TouchableOpacity >
            </ImageBackground>
        );
    }
}


