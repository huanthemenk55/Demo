import {Dimensions, StyleSheet} from "react-native";
const {width,height} = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent:"space-between"
    },
    text1: {
        fontSize:12,
        color:"black"
    },
    text2: {
        fontSize:15,
        color:"black"
    },
    text3: {
        fontSize:15,
        borderWidth:0.5 ,
        borderColor:"black",
        borderRadius:15,
        padding:height/100,
        width:width-width/12,
        height:height/20,
        color:"black"

    },
    text4: {
        fontSize:15,
        borderWidth:1,borderColor:"black",
        backgroundColor:"green",
        borderRadius:10,
        paddingLeft:width*0.38,
        paddingTop:height/100,
        color:"white",
        width:width-width/20,
        height:height/20,
    },
    view1: {
        justifyContent:"center",
        alignItems:"center",
        height:height/13
    },
    view2: {
        flexDirection:"row",
        height:height/13
    },
    view3: {
        justifyContent:"center",
        alignItems:"center",
        height:4*height/13
    },
    view4: {
        justifyContent:"space-between",
        alignItems:"center",
        height:3*height/13

    },
    view5: {
        padding:height/18,
        height:3*height/13,
        justifyContent:"space-between",
        alignItems:"center",
    },
});
export default styles;